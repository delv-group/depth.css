# ![](assets/depthcss-logo@32.png) Depth.css

CSS framework for delv

## 💗 Status

- [x] Concept
- [x] Prototype
- [ ] Alpha
- [ ] Beta
- [ ] Release

## 🏗 Build

Build with latest dart-sass
```
sass depth.scss depth.css
```

### Don't have sass?

#### Windows
```
choco install sass
```
***- or -***
```
scoop bucket add main
scoop install main/sass
```
#### Others
```
brew install sass/sass/sass
```
#### More
https://sass-lang.com/install/
